const userData = require(`./2-arrays-logins.cjs`);

const comparator = (nameA, nameB) => nameA.firstName > nameB.firstName ? -1 : 0;

userData.sort(comparator);

console.log(userData);
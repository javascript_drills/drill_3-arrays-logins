const userData = require(`./2-arrays-logins.cjs`);

const mailCounts = {

    ".org": 0,

    ".au": 0,

    ".com": 0

}

for (let index = 0; index < userData.length; index++) {

    let length = userData[index]["email"].length;

    let domain = userData[index]["email"];

    if (domain.endsWith(".org")) {

        mailCounts[".org"] += 1;

    } else if (domain.endsWith(".au")) {

        mailCounts[".au"] += 1;
    } else if (domain.endsWith(".com")) {

        mailCounts[".com"] += 1;
    }
}


console.log(mailCounts);
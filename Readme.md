# Drill on Arrays of objects.


## Drill contains Question on Array of objects.


### following are the Questions in the Drill.

1. Sorting users with particular gender.

2. Splitting IP address into its components.

3. Sum of particular Component of the IP address.

4. Computing full name of user adding new key as full name and its value.

5. Filtering emails with ".org" as their Domain.

6. Calculating the counts of various Domain from users data.

7. Sorting users first name in reverse Alphabetical order.